from django.contrib import admin
from todos.models import TodoList, TodoItem
# Register your models here.


class ToDoListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


admin.site.register(TodoList, ToDoListAdmin)


class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
    )


admin.site.register(TodoItem, TodoItemAdmin)
